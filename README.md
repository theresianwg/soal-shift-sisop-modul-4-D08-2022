# soal-shift-sisop-modul-4-D08-2022

|          Nama          |      NRP   |
| :------------------    | :----------|
| Theresia Nawangsih     | 5025201144 |

## Kendala :
diawal sempat seperti dibawah ini

![kendala](https://drive.google.com/uc?export=view&id=1eM4flxiS2uc6ilsJL6ThTDWFTztvw-sR)



# Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system 

### A. Semua direktori dengan awalan `Animeku_` akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan `atbash` cipher dan huruf kecil akan terencode dengan `rot13`
Pertama,  membuat 
```
bool Animeku(const char *path) 
{
    for(int i=0;i<strlen(path)-8+1;i++)
        if(path[i] == 'A' && path[i+1] == 'n' && path[i+2] == 'i' && path[i+3] == 'm' && path[i+4] == 'e'
        && path[i+5] == 'k' && path[i+6] == 'u' && path[i+7] == '_') return 1;
    return 0;
}
```
untuk mengetahui nilai true dan false. Jika bernilai true maka akan terencode dengan menggunakan
```
void encodeRot(char *s)
{
    for(int i=0;i<strlen(s);i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
}

```
dan jika bernilai false, maka akam terdecode
```
void decodeRot(char *s)
{
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
        else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
}
```

### B. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
Pada soal ini dapat memanggil ` xmp_rename` yang digunakan untuk mengganti nama file maupun direktori. Selanjutnya, akan mendeklarasikan string pada nama direktori sebelum dan sesudahnya akan terencode jika memiliki substring `Animeku_`, dan sebaliknya jika tidak memiliki akan terdecode
```
 else if (Animeku(fpath) && !Animeku(tpath)) 
        {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int itung = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", total);
        }
    
        else if (!Animeku(fpath) && Animeku(tpath)) 
        {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int itung = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", total)
```

### C. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
Pada soal ini sama dengan nomo 1b. Maka, pertama dapat memanggil ` xmp_rename` yang digunakan untuk mengganti nama file maupun direktori. Selanjutnya, akan mendeklarasikan string pada nama direktori sebelum dan sesudahnya akan terencode jika memiliki substring `Animeku_`, dan sebaliknya jika tidak memiliki akan terdecode
```
 else if (Animeku(fpath) && !Animeku(tpath)) 
        {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int itung = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", total);
        }
    
        else if (!Animeku(fpath) && Animeku(tpath)) 
        {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int itung = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", total)
```


### D. Setiap data yang terencode akan masuk dalam file “Wibu.log”
Pertama,  akan membuat `logRename` 
```
void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;

```
yang nanti nya akan terintegrasi dengan fungsi `sistemLog` 
```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }
        else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
        else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 
    
}
```
Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi.


### E. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
Pertama, kita akan membuat sebuah fungsi encode dan decode sebuah direktori (rekursif)
```
int encodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int total=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            total += encodeFolderRekursif(path, depth - 1),
            encodeFolder(basePath, dp->d_name);
        else if(encodeFile(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}
```
```
int decodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int total = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            total += decodeFolderRekursif(path, depth - 1),
            decodeFolder(basePath, dp->d_name);
        else if(decodeFile(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}
```
fungsi `encodeFolderRekursif` dan `decodeFolderRekursif` untuk melakukan sebuah proses scan file dan folder. Proses decode maupun encode akan dilanjutkan pada suatu folder yang berhasil ditemukan berdasarkan parameter basePath yang dimasukkan saat pemanggilan fungsi.

- DOKUMENTASI
![run1](https://drive.google.com/uc?export=view&id=1l7ivIWC4Cdo8Exwe9w0wIqDdbWL1IPMY)

![LOG1](https://drive.google.com/uc?export=view&id=1Q_jRVYkYAWOVYiXw3un4ak7y3ua-fT1R)



# Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut 

### A. Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).
Pertama,  membuat 
```
bool IAN(const char *path) 
{
    for(int i=0;i<strlen(path)-4+1;i++)
        if(path[i] == 'I' && path[i+1] == 'A' && path[i+2] == 'N' && path[i+3] == '_') return 1;
    return 0;
}
```
untuk mengetahui nilai true dan false. Jika bernilai true maka akan terencode dengan menggunakan
```
void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}
```
dan jika bernilai false, maka akam terdecode
```
void decodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'A';
        else if ('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'a';
}
```

### B. Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
Pada soal ini, dapat memanggil  ` xmp_rename` yang digunakan untuk mengganti nama file maupun direktori. Selanjutnya, akan mendeklarasikan string pada nama direktori sebelum dan sesudahnya akan terencode jika memiliki substring , dan sebaliknya jika tidak memiliki akan terdecode
```
else if(IAN(fpath) && !IAN(tpath)){
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int total = decodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", total);
        }
        else if(!IAN(fpath) && IAN(tpath)){
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int total = encodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", total);
        }
```

### C. Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
Pada soal ini sama dengan nomor 2b. Maka, pertama dapat memanggil ` xmp_rename` yang digunakan untuk mengganti nama file maupun direktori. Selanjutnya, akan mendeklarasikan string pada nama direktori sebelum dan sesudahnya akan terencode jika memiliki substring , dan sebaliknya jika tidak memiliki akan terdecode
```
else if(IAN(fpath) && !IAN(tpath)){
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int total = decodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", total);
        }
        else if(!IAN(fpath) && IAN(tpath)){
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int total = encodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", total);
        }
```

### D. Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.
Pertama,  akan membuat `logInfo` 
```
void logInfo(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "INFO::%s:%s::%s", waktu, cmd, des);
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

```
yang nanti nya akan terintegrasi dengan fungsi `sistemLog` 
```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }
        else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
        else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 
    
}
```
Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi.

### E. Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

` [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC] `

Pertama,  akan membuat `logWarning` 
```
void logWarning(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t); 
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
    char logNya[1100];
    sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
}

```
yang nanti nya akan terintegrasi dengan fungsi `sistemLog` 
```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }
        else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
        else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 
    
}
```
- DOKUMENTASI
![run2](https://drive.google.com/uc?export=view&id=1CWso3uRY_odm1-TOWdEAkiJNODpJZFez)
![RUN22](https://drive.google.com/uc?export=view&id=1WkVhZz09hzTF6kVqnWFv4NVOsmLmfoGR)

![LOG2](https://drive.google.com/uc?export=view&id=1-O2-bPjDb_MD5NglJPCiylmdAec_E_Hw)



# Soal 3
Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :.

### A. Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial
Pertama,  membuat untuk mengetahui nilai true dan false.
```
bool Namdosaq(const char *path) 
{
    for(int i=strlen(path)-1;i>=11;i--)
        if (path[i-11] == 'n' && path[i-10] == 'a' && path[i-9] == 'm' && path[i-8] == '_' &&
            path[i-7] == 'd' && path[i-6] == 'o' && path[i-5] == '-' && path[i-4] == 's' &&
            path[i-3] == 'a' && path[i-2] == 'q' && path[i-1] == '_') return 1;
    return 0;
}
```
Selanjutnya, akan membuat 
```
void encryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat path_stat;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char newFilePath[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &path_stat) < 0);
		else if (S_ISDIR(path_stat.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0){
                continue;
                printf(dirPath,"%s/%s",filepath, dp->d_name);
                encryptSpecial(dirPath);
            }
```

### B. Jika suatu direktori di-rename dengan memberi awalan `nam_do-saq_`,maka direktori tersebut akan menjadi sebuah direktori spesial.
Pada soal ini dapat memanggil ` xmp_rename` yang digunakan untuk mengganti nama file maupun direktori. Selanjutnya, akan mendeklarasikan string pada nama direktori sebelum dan sesudahnya akan terencode jika memiliki substring `nam_do-saq_`, dan sebaliknya jika tidak memiliki akan terdecode
```
 else if (Namdosaq(fpath) && Animeku(tpath))
        {
            encryptSpecial(fpath);
        }
       
       
        else if (Namdosaq(fpath) && IAN(tpath))
        {
            encryptSpecial(fpath);
        }

       
        else if (Animeku(fpath) && Namdosaq(tpath))
        {
            decryptSpecial(fpath);
            sistemLog(fpath, tpath, 2);
        }

       
        else if (IAN(fpath) && Namdosaq(tpath))
        {
            decryptSpecial(fpath);
            sistemLog(fpath, tpath, 2);
        }
    }
    res=rename(fpath, tpath);
    if(res == -1) return -errno;
    return 0;
}
```

### C. Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

### D. Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).

Pertama, kita akan membuat sebuah fungsi encode dan decode sebuah direktori (rekursif)
```
int encodeFolderRekursifIAN(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int total=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            total += encodeFolderRekursifIAN(path, depth - 1),
            encodeFolderIAN(basePath, dp->d_name);
        else if(encodeFileIAN(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}
```
```
int decodeFolderRekursifIAN(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int total = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            total += decodeFolderRekursifIAN(path, depth - 1),
            decodeFolderIAN(basePath, dp->d_name);
        else if(decodeFileIAN(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}
```
fungsi `encodeFolderRekursifIAN` dan `decodeFolderRekursifIAN` untuk melakukan sebuah proses scan file dan folder. Proses decode maupun encode akan dilanjutkan pada suatu folder yang berhasil ditemukan berdasarkan parameter basePath yang dimasukkan saat pemanggilan fungsi

### E. Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

Pertama,  akan membuat `void getBiner`
```
void getBiner(char *path, char *biner, char *lowercase){
	int idFirst = 0;
    for(int i=0; i<strlen(path); i++){
		if (path[i] == '/'){ 
            idFirst = i + 1;
        }
	}
    
    int idEnd = strlen(path);
    for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){ 
            idEnd = 1;
        }
	}

	int i;
	for(i=idFirst; i<idEnd; i++){
		if(isupper(path[i])){
			biner[i] = '1';lowercase[i] = path[i] + 32;
		}
		else{
			biner[i] = '0';lowercase[i] = path[i];
		}
	}
	biner[idEnd] = '\0';
	
	for(; i<strlen(path); i++){
		lowercase[i] = path[i];
	}
	lowercase[i] = '\0';
}
```
Dilanjutkan dengan mengubah nilai desimal dari biner
```
int convertDecimal(char *ext){
	int decimal = 0, temp = 1;
	for(int i=strlen(ext)-1; i>=0; i--){
        decimal += (ext[i]-'0')*temp;
        temp *= 10;
    }
	return decimal;
}
```
