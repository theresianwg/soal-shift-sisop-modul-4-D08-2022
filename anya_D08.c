#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <fuse.h>
#include <dirent.h>


static const char *dirpath  = "/home/theresianwg/Documents";
static const char *fileLogHayo = "/home/theresianwg/hayolongapain_D08.log";
static const char *fileLog = "/home/theresianwg/Wibu.log";

bool Animeku(const char *path) 
{
    for(int i=0;i<strlen(path)-8+1;i++)
        if(path[i] == 'A' && path[i+1] == 'n' && path[i+2] == 'i' && path[i+3] == 'm' && path[i+4] == 'e'
        && path[i+5] == 'k' && path[i+6] == 'u' && path[i+7] == '_') return 1;
    return 0;
}

bool IAN(const char *path) 
{
    for(int i=0;i<strlen(path)-4+1;i++)
        if(path[i] == 'I' && path[i+1] == 'A' && path[i+2] == 'N' && path[i+3] == '_') return 1;
    return 0;
}

bool Namdosaq(const char *path) 
{
    for(int i=strlen(path)-1;i>=11;i--)
        if (path[i-11] == 'n' && path[i-10] == 'a' && path[i-9] == 'm' && path[i-8] == '_' &&
            path[i-7] == 'd' && path[i-6] == 'o' && path[i-5] == '-' && path[i-4] == 's' &&
            path[i-3] == 'a' && path[i-2] == 'q' && path[i-1] == '_') return 1;
    return 0;
}

// Encode Decode dengan Atbash + ROT13

void encodeRot(char *s)
{
    for(int i=0;i<strlen(s);i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
}

void decodeRot(char *s)
{
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
        else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
}


void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}

void decodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'A';
        else if ('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'a';
}


void logInfo(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "INFO::%s:%s::%s", waktu, cmd, des);
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

void logWarning(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t); 
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
    char logNya[1100];
    sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
}

void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }
        else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
        else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 
    
}

void detailFileNya(const char *namaFileLengkap, char *nama, char *ekstensi) 
{
    int id=0, i=0;
    while(namaFileLengkap[i]) 
    {
        if(namaFileLengkap[i] == '.') break;
        nama[id++] = namaFileLengkap[i++];
    }
    nama[id] = '\0';
    id = 0;
    while(namaFileLengkap[i]) ekstensi[id++] = namaFileLengkap[i++];
    ekstensi[id] = '\0';
}

int encodeFolder(const char *basePath, const char* folderName) 
{
    char encryptedName[512];
    strcpy(encryptedName, folderName);
    encodeRot(encryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, encryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}


int encodeFolderIAN(const char *basePath, const char* folderName) 
{
    char encryptedName[512];
    strcpy(encryptedName, folderName);
    encodeVig(encryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, encryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFolder(const char *basePath, const char* folderName) 
{
    char decryptedName[512];
    strcpy(decryptedName, folderName);
    decodeRot(decryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, decryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}


int decodeFolderIAN(const char *basePath, const char* folderName) 
{
    char decryptedName[512];
    strcpy(decryptedName, folderName);
    decodeVig(decryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, decryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int encodeFile(char *basePath, char *name) 
{
    char fileName[512], ext[64];
    detailFileNya(name, fileName, ext);
    encodeRot(fileName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}


int encodeFileIAN(char *basePath, char *name) 
{
    char fileName[512], ext[64];
    detailFileNya(name, fileName, ext);
    encodeVig(fileName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFile(char *basePath, char *name) 
{
    char fileName[1024], ext[64];
    detailFileNya(name, fileName, ext);
    decodeRot(fileName);
    char f_path[1024], t_path[1100];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}


int decodeFileIAN(char *basePath, char *name) 
{
    char fileName[1024], ext[64];
    detailFileNya(name, fileName, ext);
    decodeVig(fileName);
    char f_path[1024], t_path[1100];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int encodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int total=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            total += encodeFolderRekursif(path, depth - 1),
            encodeFolder(basePath, dp->d_name);
        else if(encodeFile(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}


int encodeFolderRekursifIAN(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int total=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            total += encodeFolderRekursifIAN(path, depth - 1),
            encodeFolderIAN(basePath, dp->d_name);
        else if(encodeFileIAN(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}

int decodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int total = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            total += decodeFolderRekursif(path, depth - 1),
            decodeFolder(basePath, dp->d_name);
        else if(decodeFile(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}


int decodeFolderRekursifIAN(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int total = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            total += decodeFolderRekursifIAN(path, depth - 1),
            decodeFolderIAN(basePath, dp->d_name);
        else if(decodeFileIAN(basePath, dp->d_name) == 0) total++;
    }
    closedir(dir);
    return total;
}

void getBiner(char *path, char *biner, char *lowercase){
	int idFirst = 0;
    for(int i=0; i<strlen(path); i++){
		if (path[i] == '/'){ 
            idFirst = i + 1;
        }
	}
    
    int idEnd = strlen(path);
    for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){ 
            idEnd = 1;
        }
	}

	int i;
	for(i=idFirst; i<idEnd; i++){
		if(isupper(path[i])){
			biner[i] = '1';lowercase[i] = path[i] + 32;
		}
		else{
			biner[i] = '0';lowercase[i] = path[i];
		}
	}
	biner[idEnd] = '\0';
	
	for(; i<strlen(path); i++){
		lowercase[i] = path[i];
	}
	lowercase[i] = '\0';
}

int binerToDecimal(char *biner){
	int temp = 1, result = 0;
	for(int i=strlen(biner)-1; i>=0; i--){
        if(biner[i] == '1'){
            result += temp; temp *= 2;
        }
    }
	return result;
}

void encryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat path_stat;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char newFilePath[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &path_stat) < 0);
		else if (S_ISDIR(path_stat.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0){
                continue;
                printf(dirPath,"%s/%s",filepath, dp->d_name);
                encryptSpecial(dirPath);
            }
		}
        else{
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char biner[1000], lowercase[1000]; 
            getBiner(dp->d_name, biner, lowercase);
			int decimal = binerToDecimal(biner);
			printf(newFilePath,"%s/%s.%d",filepath,lowercase,decimal); 
            rename(filePath, newFilePath);
		}
	}
    closedir(dir);
}

int convertDecimal(char *ext){
	int decimal = 0, temp = 1;
	for(int i=strlen(ext)-1; i>=0; i--){
        decimal += (ext[i]-'0')*temp;
        temp *= 10;
    }
	return decimal;
}

void decimalToBiner(int decimal, char *biner, int len){
	int idx = 0;
	while(decimal){
		if(decimal & 1){
            biner[idx] = '1';
        }
		else {
            biner[idx] = '0';
            idx++;
        }
		decimal /= 2;
	}
	while(idx < len){
		biner[idx] = '0'; idx++;
	}
	biner[idx] = '\0';
	
	for(int i=0; i<idx/2; i++){
		char temp = biner[i];
        biner[i] = biner[idx-1-i];
        biner[idx-1-i] = temp;
	}
}

void getDecimal(char *path, char *biner, char *normalcase){
	int idFirst = 0;
    for(int i=0; i<strlen(path); i++){
		if (path[i] == '/'){ 
            idFirst = i + 1;
        }
	}
    
    int idEnd = strlen(path);
    for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){ 
            idEnd = 1;
        }
	}
	int i;
	
	for(i=idFirst; i<idEnd; i++){
		if(biner[i-idFirst] == '1') normalcase[i-idFirst] = path[i] - 32;
		else normalcase[i-idFirst] = path[i];
	}
	
	for(; i<strlen(path); i++){
		normalcase[i-idFirst] = path[i];
	}
	normalcase[i-idFirst] = '\0';
}

void decryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat stat_path;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char newFilePath[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &stat_path) < 0);
		else if (S_ISDIR(stat_path.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0) continue;
            sprintf(dirPath,"%s/%s",filepath, dp->d_name);
            decryptSpecial(dirPath);
		}
		else{
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dp->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertDecimal(ext+1);
			for(int i=0; i<strlen(fname)-strlen(ext); i++) clearPath[i] = fname[i];
			
			char *ext2 = strrchr(clearPath, '.');
			decimalToBiner(dec, bin, strlen(clearPath)-strlen(ext2));
            getDecimal(clearPath, bin, normalcase);
            printf(newFilePath,"%s/%s",filepath,normalcase);
            rename(filePath, newFilePath);
		}
	}
    closedir(dir);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    res = lstat(fpath, stbuf);
    if(res == -1) return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path);
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    dp = opendir(fpath);
    if (dp == NULL) return -errno;
    while((de = readdir(dp)) != NULL) 
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino; st.st_mode = de->d_type << 12;
        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path); 
    int fd = open(fpath, O_RDONLY), res;
    (void) fi;
    if (fd == -1) return -errno; 
    res = pread(fd, buf, size, offset); 
    if(res == -1)res = -errno; 
    close(fd);
    return res;
}

static int xmp_rename(const char *from, const char *to) 
{
    int res; 
    char fpath[1000], tpath[1000];
    if (strcmp(from, "/") == 0) from = dirpath, sprintf(fpath, "%s", from);
    else sprintf(fpath, "%s%s", dirpath, from);
    if (strcmp(to, "/") == 0) to = dirpath, sprintf(tpath, "%s", to);
    else sprintf(tpath, "%s%s", dirpath, to); 
    struct stat path_stat;
    stat(fpath, &path_stat);
    if (!S_ISREG(path_stat.st_mode)) 
    {
        // jika folder terencode dan mau didecode & rename Animeku ke IAN
        if (Animeku(fpath) && Namdosaq(tpath)) 
        {
            decodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        else if(Animeku(fpath) && IAN(tpath)){
            decodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        
        // Rename 
        else if (IAN(fpath) && Animeku(tpath)){
            encodeFolderRekursif(fpath,0);
            sistemLog(fpath,tpath,1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if (Namdosaq(fpath) && Animeku(tpath)) 
        {
            encodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if (Animeku(fpath) && !Animeku(tpath)) 
        {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int total = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", total);
        }
        
        // folder decode encode
        else if (!Animeku(fpath) && Animeku(tpath)) 
        {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int total = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", total);
      
      
        }
        else if(Animeku(fpath) && IAN(tpath)){
            encodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath,tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if(Namdosaq(fpath) && IAN(tpath)){
            encodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath,tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if(IAN(fpath) && Animeku(tpath)){
            decodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        else if(IAN(fpath) && Namdosaq(tpath)){
            decodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        else if(IAN(fpath) && !IAN(tpath)){
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int total = decodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", total);
        }
        else if(!IAN(fpath) && IAN(tpath)){
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int total = encodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", total);
        }

        //rename
        
        else if (Namdosaq(fpath) && Animeku(tpath))
        {
            encryptSpecial(fpath);
        }
       
       
        else if (Namdosaq(fpath) && IAN(tpath))
        {
            encryptSpecial(fpath);
        }

       
        else if (Animeku(fpath) && Namdosaq(tpath))
        {
            decryptSpecial(fpath);
            sistemLog(fpath, tpath, 2);
        }

       
        else if (IAN(fpath) && Namdosaq(tpath))
        {
            decryptSpecial(fpath);
            sistemLog(fpath, tpath, 2);
        }
    }
    res=rename(fpath, tpath);
    if(res == -1) return -errno;
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path);
	int res;  
	if (S_ISREG(mode)) 
    {
		res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0) res = close(res);
	} 
    else if (S_ISFIFO(mode)) res = mkfifo(fpath, mode);
	else res = mknod(fpath, mode, rdev);
	if (res == -1) return -errno; 
	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode) 
{
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path);
    res = mkdir(fpath, mode);
    if (res == -1) return -errno;
    if (Animeku(fpath)) sistemLog("", fpath, 3);
    else if(IAN(fpath)) sistemLog("", fpath, 3);
    return 0;
}

static int xmp_rmdir(const char *path){
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path);
    res = rmdir(fpath);
    if (res == -1) return -errno;
    sistemLog("",fpath, 4);
    return 0;
}

static int xmp_unlink(const char *path){
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path);
    res = unlink(fpath);
    if (res == -1) return -errno;
    sistemLog("",fpath, 5);
    return 0;
}

static const struct fuse_operations xmp_oper = 
{
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .mknod = xmp_mknod,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
